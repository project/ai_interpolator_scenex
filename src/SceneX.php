<?php

namespace Drupal\ai_interpolator_scenex;

use Drupal\ai_interpolator_scenex\Form\SceneXConfigForm;
use Drupal\Core\Config\ConfigFactory;
use GuzzleHttp\Client;

/**
 * SceneX API creator.
 */
class SceneX {

  /**
   * The http client.
   */
  protected Client $client;

  /**
   * API Key.
   */
  private string $apiKey;

  /**
   * The base path.
   */
  private string $basePath = 'https://api.scenex.jina.ai/v1/';

  /**
   * Constructs a new SceneX object.
   *
   * @param \GuzzleHttp\Client $client
   *   Http client.
   * @param \Drupal\Core\Config\ConfigFactory $configFactory
   *   The config factory.
   */
  public function __construct(Client $client, ConfigFactory $configFactory) {
    $this->client = $client;
    $this->apiKey = $configFactory->get(SceneXConfigForm::CONFIG_NAME)->get('api_key') ?? '';
  }

  /**
   * Describe images from text.
   *
   * @param array $images
   *   Images in base64 encoding.
   *
   * @return string
   *   The description.
   */
  public function describe(array $images) {
    if (!$this->apiKey) {
      return [];
    }
    foreach ($images as $image) {
      $data['data'][] = [
        'image' => $image,
        'features' => [],
      ];
    }
    $guzzleOptions['headers']['Content-Type'] = 'application/json';
    return $this->makeRequest("describe", [], 'POST', json_encode($data), $guzzleOptions)->getContents();
  }

  /**
   * Make SceneX call.
   *
   * @param string $path
   *   The path.
   * @param array $query_string
   *   The query string.
   * @param string $method
   *   The method.
   * @param string $body
   *   Data to attach if POST/PUT/PATCH.
   * @param array $options
   *   Extra headers.
   *
   * @return string|object
   *   The return response.
   */
  protected function makeRequest($path, array $query_string = [], $method = 'GET', $body = '', array $options = []) {
    // We can wait some.
    $options['connect_timeout'] = 30;
    $options['read_timeout'] = 30;
    // Don't let Guzzle die, just forward body and status.
    $options['http_errors'] = FALSE;
    // Headers.
    $options['headers']['x-api-key'] = ' ' . $this->apiKey;

    if ($body) {
      $options['body'] = $body;
    }

    $new_url = $this->basePath . $path;
    $new_url .= count($query_string) ? '?' . http_build_query($query_string) : '';

    $res = $this->client->request($method, $new_url, $options);

    return $res->getBody();
  }

}
